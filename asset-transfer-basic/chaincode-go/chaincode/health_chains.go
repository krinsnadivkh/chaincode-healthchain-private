package chaincode

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type SmartContract struct {
	contractapi.Contract
}

type User struct {
	Username string `json:"username"`
	IDCard   string `json:"idcard"`
	Email    string `json:"email"`
	Role     string `json:"role"`
	Profile  string `json:"profile"`
}

type Medical struct {
	MedicineName  string `json:"medicienname"`
	UsingDuration string `json:"usingduration"`
	Price         int    `json:"price"`
	Description   string `json:"description"`
	Qty           string `json:"qty"`
}

type Doctor struct {
	Fullname string `json:"fullname"`
	IDCard   string `json:"idcard"`
	Position string `json:"position"`
	Phone    string `json:"phone"`
	Profile  string `json:"profile"`
}

type Hospital struct {
	Name    string `json:"name"`
	Address string `json:"address"`
	Phone   string `json:"phone"`
	Profile string `json:"profile"`
}

type Record struct {
	RecordToken string    `json:"recordtoken"` //is the record id
	Patient     User      `json:"patient"`
	Status      string    `json:"status"`
	Medical     []Medical `json:"medical"`
	Condition   string    `json:"condition"`
	Description string    `json:"description"`
	Doctor      Doctor    `json:"doctor"`
	Hospital    Hospital  `json:"hospital"`
	CreatedDate string    `json:"createddate"`
	Subtotal    float32   `json:"subtotal"`
}

// ==========================================Function section ==========================================

// InitLedger adds a base set of assets to the ledger
func (t *SmartContract) InitRecord(ctx contractapi.TransactionContextInterface) error {
	user := User{
		Username: "Krisna", IDCard: "222", Email: "@gmail.com", Role: "User", Profile: "url",
	}

	doc := Doctor{
		Fullname: "Sokea", Position: "Doctor", Phone: "09754123", Profile: "url",
	}
	hos := Hospital{
		Name: "HRD", Address: "PP", Phone: "24845145", Profile: "url",
	}

	medical := []Medical{{
		MedicineName: "paranadol", UsingDuration: "5 day", Price: 200, Description: "This patient is handsome ", Qty: "okay",
	}, {
		MedicineName: "gogole", UsingDuration: "5 day", Price: 200, Description: "haha ", Qty: "555",
	}}

	record := []Record{
		{
			RecordToken: "asset1",
			Patient:     user,
			Status:      "true",
			Condition:   "Good",
			Description: "nice",
			Doctor:      doc,
			Medical:     medical,
			Hospital:    hos,
			CreatedDate: "528/2022",
			Subtotal:    200,
		}, {
			RecordToken: "asset3",
			Patient:     user,
			Status:      "true",
			Condition:   "Good",
			Description: "nice",
			Doctor:      doc,
			Medical:     medical,
			Hospital:    hos,
			CreatedDate: "528/2022",
			Subtotal:    300,
		}}

	for _, record := range record {
		assetJSON, err := json.Marshal(record)
		if err != nil {
			return err
		}

		err = ctx.GetStub().PutState(record.RecordToken, assetJSON)
		if err != nil {
			return fmt.Errorf("failed to put to world state. %v", err)
		}
	}

	return nil
}

// create record
func (s *SmartContract) CreateRecord(ctx contractapi.TransactionContextInterface, recordtoken string, patient User, status string, medical []Medical, condition string, description string, doctor Doctor, hospital Hospital, createdDate string, subTotal float32) error {
	exists, err := s.RecordExists(ctx, recordtoken)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("the record %s already exists", recordtoken)
	}
	record := Record{
		RecordToken: recordtoken,
		Patient:     patient,
		Status:      status,
		Medical:     medical,
		Condition:   condition,
		Description: description,
		Doctor:      doctor,
		Hospital:    hospital,
		CreatedDate: createdDate,
		Subtotal:    subTotal,
	}
	recordJSON, err := json.Marshal(record)
	if err != nil {
		return err
	}
	return ctx.GetStub().PutState(recordtoken, recordJSON)
}

// get record by given id
func (s *SmartContract) ReadRecordById(ctx contractapi.TransactionContextInterface, id string) (*Record, error) {
	recordJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if recordJSON == nil {
		return nil, fmt.Errorf("the asset %s does not exist", id)
	}
	var record Record
	err = json.Unmarshal(recordJSON, &record)
	if err != nil {
		return nil, err
	}
	return &record, nil
}

// DeleteAsset deletes an given asset from the world state.
func (s *SmartContract) DeleteRecordById(ctx contractapi.TransactionContextInterface, recordId string) error {
	exists, err := s.RecordExists(ctx, recordId)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the record %s does not exist", recordId)
	}
	return ctx.GetStub().DelState(recordId)
}

// update record
func (s *SmartContract) UpdateRecordById(ctx contractapi.TransactionContextInterface, recordId string, patient User, status string, medical []Medical, condition string, description string, doctor Doctor, hospital Hospital, createdDate string, subTotal float32) (string, error) {
	var msg string
	record, err := s.ReadRecordById(ctx, recordId)
	record.Status = status
	record.Patient = patient
	record.Medical = medical
	record.Condition = condition
	record.Doctor = doctor
	record.Hospital = hospital
	record.CreatedDate = createdDate
	record.Subtotal = subTotal
	assetJSON, err := json.Marshal(record)
	if err == nil {
		err = ctx.GetStub().PutState(recordId, assetJSON)
		msg = "Record update successfully"
	} else {
		msg = "Fail to update record"
	}
	return msg, err
}

// Update Record asset status
func (s *SmartContract) UpdateRecordStatus(ctx contractapi.TransactionContextInterface, recordId string, status string) (string, error) {
	var msg string
	record, err := s.ReadRecordById(ctx, recordId)
	if err != nil {
		return "", err
	} else {
		record.Status = status
	}
	recordJSON, err := json.Marshal(record)
	if err != nil {
		return "", err
	}
	err = ctx.GetStub().PutState(recordId, recordJSON)
	if err != nil {
		return "", err
	} else {
		msg = "Record status update successfully"
	}

	return msg, nil

}

// get all record form user ID card which show only public record
func (t *SmartContract) QueryRecordByIdCardForUser(ctx contractapi.TransactionContextInterface, idcard string) ([]*Record, error) {
	queryString := fmt.Sprintf(`{"selector":{"patient":{"idcard":"%s"}}}`, idcard)
	return getQueryResultForQueryStringForUser(ctx, queryString)
}

func getQueryResultForQueryStringForUser(ctx contractapi.TransactionContextInterface, queryString string) ([]*Record, error) {
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	return constructQueryResponseFromIteratorForUser(resultsIterator)
}

func constructQueryResponseFromIteratorForUser(resultsIterator shim.StateQueryIteratorInterface) ([]*Record, error) {
	var records []*Record

	for resultsIterator.HasNext() {
		queryResult, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		var record Record
		err = json.Unmarshal(queryResult.Value, &record)
		if err != nil {
			return nil, err
		}
		if record.Status != "false" {
			records = append(records, &record)
		}
	}
	return records, nil
}

// get all record by user ID card which show public and private for owner
func (t *SmartContract) QueryRecordByIdCardForOwner(ctx contractapi.TransactionContextInterface, idcard string) ([]*Record, error) {
	queryString := fmt.Sprintf(`{"selector":{"patient":{"idcard":"%s"}}}`, idcard)
	return getQueryResultForQueryStringForOwner(ctx, queryString)
}

func getQueryResultForQueryStringForOwner(ctx contractapi.TransactionContextInterface, queryString string) ([]*Record, error) {
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	return constructQueryResponseFromIteratorForOwner(resultsIterator)
}
func constructQueryResponseFromIteratorForOwner(resultsIterator shim.StateQueryIteratorInterface) ([]*Record, error) {
	var records []*Record

	for resultsIterator.HasNext() {
		queryResult, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		var record Record
		err = json.Unmarshal(queryResult.Value, &record)
		if err != nil {
			return nil, err
		}
		records = append(records, &record)
	}
	return records, nil
}

// get all total expense by user id

func (t *SmartContract) QueryTotalExpenseforUser(ctx contractapi.TransactionContextInterface, idcard string) (float32, error) {
	queryString := fmt.Sprintf(`{"selector":{"patient":{"idcard":"%s"}}}`, idcard)
	return getQueryExpenseForOwner(ctx, queryString)
}

func getQueryExpenseForOwner(ctx contractapi.TransactionContextInterface, queryString string) (float32, error) {
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		// return nil, err
	}
	defer resultsIterator.Close()

	return CaculateTotalExpense(resultsIterator)
}

func CaculateTotalExpense(resultsIterator shim.StateQueryIteratorInterface) (float32, error) {
	var records []*Record
	var total float32
	for resultsIterator.HasNext() {
		queryResult, err := resultsIterator.Next()
		if err != nil {
			// return nil, err
		}
		var record Record
		err = json.Unmarshal(queryResult.Value, &record)
		if err != nil {
			// return nil, err
		}
		total = record.Subtotal + total

		records = append(records, &record)

	}

	return total, nil
}

// GetAllAssets returns all assets found in world state
func (s *SmartContract) GetAllRecordForOwner(ctx contractapi.TransactionContextInterface) ([]*Record, error) {
	resultsIterator, err := ctx.GetStub().GetStateByRange("", "")
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var records []*Record
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}

		var record Record
		err = json.Unmarshal(queryResponse.Value, &record)
		if err != nil {
			return nil, err
		}
		records = append(records, &record)
	}

	return records, nil
}

// AssetExists returns true when asset with given ID exists in world state
func (s *SmartContract) RecordExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}
	return assetJSON != nil, nil
}

// get all user by doctor id (when we want to know how many patient's record have record by doctor)
func (t *SmartContract) GetAllUserByDoctorID(ctx contractapi.TransactionContextInterface, idcard string) ([]*User, error) {
	queryString := fmt.Sprintf(`{"selector":{"doctor":{"idcard":"%s"}}}`, idcard)
	return queryResultForQueryStringForUser(ctx, queryString)
}

func queryResultForQueryStringForUser(ctx contractapi.TransactionContextInterface, queryString string) ([]*User, error) {
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	return constructCountUserByIDCard(resultsIterator)
}

func constructCountUserByIDCard(resultsIterator shim.StateQueryIteratorInterface) ([]*User, error) {
	// var records []*Record
	var users []*User

	for resultsIterator.HasNext() {
		queryResult, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		var record Record
		err = json.Unmarshal(queryResult.Value, &record)
		if err != nil {
			return nil, err
		}
		users = append(users, &record.Patient)
	}
	return users, nil
}

// ==============================================================

// // old version
// package chaincode

// import (
// 	"encoding/json"
// 	"fmt"

// 	"github.com/hyperledger/fabric-chaincode-go/shim"
// 	"github.com/hyperledger/fabric-contract-api-go/contractapi"
// )

// type SmartContract struct {
// 	contractapi.Contract
// }

// type User struct {
// 	Username string `json:"username"`
// 	IDCard   string `json:"idcard"`
// 	Email    string `json:"email"`
// 	Role     string `json:"role"`
// }

// type Medical struct {
// 	MedicineName  string `json:"medicienname"`
// 	UsingDuration string `json:"usingduration"`
// 	Price         int    `json:"price"`

// }

// type Doctor struct {
// 	Fullname string `json:"fullname"`
// 	Position string `json:"position"`
// 	Phone    string `json:"phone"`
// }

// type Hospital struct {
// 	Name    string `json:"name"`
// 	Address string `json:"address"`
// 	Phone   string `json:"phone"`
// }

// type Record struct {
// 	RecordToken string    `json:"recordtoken"` //is the record id
// 	Patient     User      `json:"patient"`
// 	Status      string    `json:"status"`
// 	Medical     []Medical `json:"medical"`
// 	Condition   string    `json:"condition"`
// 	Description string    `json:"description"`
// 	Doctor      Doctor    `json:"doctor"`
// 	Hospital    Hospital  `json:"hospital"`
// 	CreatedDate string    `json:"createddate"`
// }

// // create record
// func (s *SmartContract) CreateRecord(ctx contractapi.TransactionContextInterface, recordtoken string, patient User, status string, medical []Medical, condition string, description string, doctor Doctor, hospital Hospital, createdDate string) error {
// 	exists, err := s.RecordExists(ctx, recordtoken)
// 	if err != nil {
// 		return err
// 	}
// 	if exists {
// 		return fmt.Errorf("the record %s already exists", recordtoken)
// 	}
// 	record := Record{
// 		RecordToken: recordtoken,
// 		Patient:     patient,
// 		Status:      status,
// 		Medical:     medical,
// 		Condition:   condition,
// 		Description: description,
// 		Doctor:      doctor,
// 		Hospital:    hospital,
// 		CreatedDate: createdDate,
// 	}
// 	recordJSON, err := json.Marshal(record)
// 	if err != nil {
// 		return err
// 	}
// 	return ctx.GetStub().PutState(recordtoken, recordJSON)
// }

// // get record by given id
// func (s *SmartContract) ReadRecordById(ctx contractapi.TransactionContextInterface, id string) (*Record, error) {
// 	recordJSON, err := ctx.GetStub().GetState(id)
// 	if err != nil {
// 		return nil, fmt.Errorf("failed to read from world state: %v", err)
// 	}
// 	if recordJSON == nil {
// 		return nil, fmt.Errorf("the asset %s does not exist", id)
// 	}
// 	var record Record
// 	err = json.Unmarshal(recordJSON, &record)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return &record, nil
// }

// // DeleteAsset deletes an given asset from the world state.
// func (s *SmartContract) DeleteRecordById(ctx contractapi.TransactionContextInterface, recordId string) error {
// 	exists, err := s.RecordExists(ctx, recordId)
// 	if err != nil {
// 		return err
// 	}
// 	if !exists {
// 		return fmt.Errorf("the record %s does not exist", recordId)
// 	}
// 	return ctx.GetStub().DelState(recordId)
// }

// // update record

// func (s *SmartContract) UpdateRecordById(ctx contractapi.TransactionContextInterface, recordId string, patient User, status string, medical []Medical, condition string, description string, doctor Doctor, hospital Hospital, createdDate string) (string, error) {
// 	record, err := s.ReadRecordById(ctx, recordId)
// 	if err != nil {
// 		// return "", err
// 	}

// 	oldStatus := record.Status
// 	record.Status = status
// 	record.Patient = patient
// 	record.Medical = medical
// 	record.Condition = condition
// 	record.Doctor = doctor
// 	record.Hospital = hospital
// 	record.CreatedDate = createdDate

// 	assetJSON, err := json.Marshal(record)
// 	if err != nil {
// 		// return "", err
// 	}

// 	err = ctx.GetStub().PutState(recordId, assetJSON)
// 	if err != nil {
// 		// return "", err
// 	}
// 	return oldStatus, nil

// }

// // Update Record asset status

// func (s *SmartContract) UpdateRecordStatus(ctx contractapi.TransactionContextInterface, recordId string, status string) (string, error) {
// 	asset, err := s.ReadRecordById(ctx, recordId)
// 	if err != nil {
// 		// return "", err
// 	}

// 	oldStatus := asset.Status
// 	asset.Status = status

// 	assetJSON, err := json.Marshal(asset)
// 	if err != nil {
// 		// return "", err
// 	}

// 	err = ctx.GetStub().PutState(recordId, assetJSON)
// 	if err != nil {
// 		// return "", err
// 	}

// 	return oldStatus, nil

// }

// // get all record form user ID card which show only public record
// func (t *SmartContract) QueryRecordByIdCardForUser(ctx contractapi.TransactionContextInterface, idcard string) ([]*Record, error) {
// 	queryString := fmt.Sprintf(`{"selector":{"patient":{"idcard":"%s"}}}`, idcard)
// 	return getQueryResultForQueryStringForUser(ctx, queryString)
// }

// func getQueryResultForQueryStringForUser(ctx contractapi.TransactionContextInterface, queryString string) ([]*Record, error) {
// 	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer resultsIterator.Close()

// 	return constructQueryResponseFromIteratorForUser(resultsIterator)
// }

// func constructQueryResponseFromIteratorForUser(resultsIterator shim.StateQueryIteratorInterface) ([]*Record, error) {
// 	var records []*Record

// 	for resultsIterator.HasNext() {
// 		queryResult, err := resultsIterator.Next()
// 		if err != nil {
// 			return nil, err
// 		}
// 		var record Record
// 		err = json.Unmarshal(queryResult.Value, &record)
// 		if err != nil {
// 			return nil, err
// 		}
// 		if record.Status != "false" {
// 			records = append(records, &record)
// 		}
// 	}
// 	return records, nil
// }

// // get all record by user ID card which show public and private for owner
// func (t *SmartContract) QueryRecordByIdCardForOwner(ctx contractapi.TransactionContextInterface, idcard string) ([]*Record, error) {
// 	queryString := fmt.Sprintf(`{"selector":{"patient":{"idcard":"%s"}}}`, idcard)
// 	return getQueryResultForQueryStringForOwner(ctx, queryString)
// }

// func getQueryResultForQueryStringForOwner(ctx contractapi.TransactionContextInterface, queryString string) ([]*Record, error) {
// 	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer resultsIterator.Close()

// 	return constructQueryResponseFromIteratorForOwner(resultsIterator)
// }
// func constructQueryResponseFromIteratorForOwner(resultsIterator shim.StateQueryIteratorInterface) ([]*Record, error) {
// 	var records []*Record

// 	for resultsIterator.HasNext() {
// 		queryResult, err := resultsIterator.Next()
// 		if err != nil {
// 			return nil, err
// 		}
// 		var record Record
// 		err = json.Unmarshal(queryResult.Value, &record)
// 		if err != nil {
// 			return nil, err
// 		}
// 		records = append(records, &record)
// 	}
// 	return records, nil
// }

// // GetAllAssets returns all assets found in world state
// func (s *SmartContract) GetAllRecordForOwner(ctx contractapi.TransactionContextInterface) ([]*Record, error) {
// 	resultsIterator, err := ctx.GetStub().GetStateByRange("", "")
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer resultsIterator.Close()

// 	var records []*Record
// 	for resultsIterator.HasNext() {
// 		queryResponse, err := resultsIterator.Next()
// 		if err != nil {
// 			return nil, err
// 		}

// 		var record Record
// 		err = json.Unmarshal(queryResponse.Value, &record)
// 		if err != nil {
// 			return nil, err
// 		}
// 		records = append(records, &record)
// 	}

// 	return records, nil
// }

// // AssetExists returns true when asset with given ID exists in world state
// func (s *SmartContract) RecordExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
// 	assetJSON, err := ctx.GetStub().GetState(id)
// 	if err != nil {
// 		return false, fmt.Errorf("failed to read from world state: %v", err)
// 	}
// 	return assetJSON != nil, nil
// }
