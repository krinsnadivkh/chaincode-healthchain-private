# init record 
peer chaincode invoke -n basic -C mychannel -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com  --tls --cafile "$ORDERER_CA" --peerAddresses localhost:7051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt  -c '{"Args":["InitRecord"]}'

# create record
peer chaincode invoke -n basic -C mychannel -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com  --tls --cafile "$ORDERER_CA" --peerAddresses localhost:7051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt  -c '{"Args":["CreateRecord", "new3","{\"email\": \"hello snanjndjk\",\"idcard\": \"3236153\",\"role\": \"user\",\"username\": \"krisna\",\"profile\": \"url\"}","true","[]","good","NO more","{  \"fullname\": \"Krisna\",\"position\": \"Doctor\", \"phone\": \"0346895656\",\"profile\": \"url\", \"idcard\": \"11111\"}","{ \"name\": \"HRD\",\"address\": \"PP\",\"phone\": \"0346895656\",\"profile\": \"url\"}","2022-20-1","555"]}'


# get total expense by user id 
peer chaincode query -C mychannel -n basic -c '{"Args":["QueryTotalExpenseforUser","222"]}'


# update recrod status 
peer chaincode invoke -n basic -C mychannel -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com  --tls --cafile "$ORDERER_CA" --peerAddresses localhost:7051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt  -c '{"Args":["UpdateRecordStatus","new3","true"]}'

# update record  by id 
peer chaincode invoke -n basic -C mychannel -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com  --tls --cafile "$ORDERER_CA" --peerAddresses localhost:7051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt  -c '{"Args":["UpdateRecordById", "new3","{\"email\": \"new@gmail.com\",\"idcard\": \"22222\",\"role\": \"admin\",\"username\": \"healthchains\",\"profile\": \"url\"}","true","[]","good","NO more","{  \"fullname\": \"Krisna\",\"position\": \"Doctor\", \"phone\": \"0346895656\",\"profile\": \"url\", \"idcard\": \"11111\"}","{ \"name\": \"HRD\",\"address\": \"PP\",\"phone\": \"0346895656\",\"profile\": \"url\"}","2022-20-1","563525"]}'


# delete record by id
peer chaincode invoke -n basic -C mychannel -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com  --tls --cafile "$ORDERER_CA" --peerAddresses localhost:7051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt  -c '{"Args":["DeleteRecordById","new3"]}'

# get public record by user id 
peer chaincode query -C mychannel -n basic -c '{"Args":["QueryRecordByIdCardForUser","222"]}'


# get public and private record by user id 

peer chaincode query -C mychannel -n basic -c '{"Args":["QueryRecordByIdCardForOwner","222"]}'

# get total expense by user id 
peer chaincode query -C mychannel -n basic -c '{"Args":["QueryTotalExpenseforUser","222"]}'


# get all user by doctor id
peer chaincode query -C mychannel -n basic -c '{"Args":["GetAllUserByDoctorID","11111"]}'




